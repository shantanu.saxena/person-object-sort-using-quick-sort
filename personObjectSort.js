class Person{
    constructor(name,age,salary,sex){
      this.name=name;
      this.age=age;
      this.salary=salary;
      this.sex=sex;
    }
 static Qsort(arr,field,order) {
        let arr2=[].concat(arr);
        
        if (arr2.length <= 1) {
            return arr2;
          }
        
          let pivot = arr2[0][field];
          let pivotItems=arr2[0];
          
          let left = []; 
          let right = [];
        
          for (let i = 1; i < arr2.length; i++) {
              if(order==='asc')
              {arr2[i][field] < pivot ? left.push(arr2[i]) : right.push(arr2[i]);}
              else
              {arr2[i][field] < pivot ? right.push(arr2[i]) : left.push(arr2[i]);}
          }
        
          return Person.Qsort(left,field,order).concat(pivotItems, Person.Qsort(right,field,order));

    }   
}
let person1=new Person('Vivek',20,65000,'M');
let person2=new Person('Ayush',21,85000,'M');
let person3=new Person('Shraddha',22,100000,'F');
let person4=new Person('Aarohi',19,50000,'F');
let person5=new Person('Kartik',24,125000,'M');
const arr=[person1,person2,person3,person4,person5];


console.log(Person.Qsort(arr,'age','asc'));
